const ethUtil = require('ethereumjs-util');
const Web3 = require('web3');
const sigUtil = require('eth-sig-util');


const initialize = () => {
    //You will start here
  };

window.addEventListener('DOMContentLoaded', initialize);

var web3 = new Web3(window.ethereum)
window.ethereum.enable().catch(error => {
    // User denied account access
    console.log(error)
});
function getAccounts(callback) {
    web3.eth.getAccounts((error, result) => {
        if (error) {
            console.log(error);
        } else {
            callback(result);
        }
    });
}

function encryptsacha() {
    getAccounts(function(accounts){
        let encryptionPublicKey;

        ethereum
            .request({
                method: 'eth_getEncryptionPublicKey',
                params: [accounts[0]], // you must have access to the specified account
            })
            .then((result) => {
                encryptionPublicKey = result;
                console.log(encryptionPublicKey);
            })
            .catch((error) => {
                if (error.code === 4001) {
                    // EIP-1193 userRejectedRequest error
                    console.log("We can't encrypt anything without the key.");
                } else {
                    console.error(error);
                }
            });
       
        /*
        const encryptedMessage = ethUtil.bufferToHex(
            Buffer.from(
                JSON.stringify(
                    sigUtil.encrypt(
                        encryptionPublicKey,
                        { data: 'Hello world!' },
                        'x25519-xsalsa20-poly1305'
                    )
                ),
                'utf8'
            )
        );*/
    });
   

}

const encryptButton = document.getElementById("encryptButton");

encryptButton.addEventListener('click', () => {
    //Will Start the metamask extension
    //ethereum.request({ method: 'eth_requestAccounts' });
    encryptsacha();
});